# vue-qrcode-reader

## Project setup

```
git clone https://wildanv10v2@bitbucket.org/wildanv10v2/qrcode-reader.git/
```
```
cd qrcode-reader
```
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
